import React from 'react';
import { Text, View, Button } from 'react-native';
import { styles } from "./App.style";

export default class App extends React.Component {
  // propriedade da classe
  name = "Charles França";

  onButtonClicked(valor) {
    // this. -> Procura propriedades/métodos dentro da classe em questão
    // alert(this.name);
    alert(valor)
  }

  render() {
    const meunome = 'Charles dos Santos França';
    // const - Depois de criada seu valor não pode ser alterado (never)
    // let - Variavel de escopo que 
    // só pode ser acessada do escopo de criação para baixo
    // var - Nivel global depois de sua criação
    /*
    * 
    */
    return (
      <View style={styles.container}>
        <Text>Meu nome é: { meunome + " texto concatenado" }.</Text>
        <Button
          title={'Botão'}
          onPress={() => this.onButtonClicked("teste de valor")}
        />
      </View>
    );
  }
}


